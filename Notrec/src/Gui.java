import java.awt.*;
import java.text.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.*;

public class Gui{
	
	int count=1, count2 = 0;
	static JFrame frame;
	
	
	public void Gui(){
		frame = new JFrame("Notrec");
		frame.setIconImage(new ImageIcon("icon.jpg").getImage());
		frame.setSize(400, 370);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setVisible(true);
		
		BorderLayout bl = new BorderLayout();
		frame.setLayout(bl);
		//
		IFile.configfile("");
		//
		JMenuBar jmbar = new JMenuBar();
		JMenu jmfile = new JMenu("File");
		JMenuItem isave = new JMenuItem("Save");
		JMenuItem iopen = new JMenuItem("Open");
		jmfile.add(isave); jmfile.add(iopen);
		jmbar.add(jmfile);
		
		JMenu jmtools = new JMenu("Tools");
		JMenuItem istats = new JMenuItem("Stats");
		jmtools.add(istats);
		jmbar.add(jmtools);
		frame.add(jmbar,bl.NORTH);
				
		iopen.addActionListener(e->{
			JFileChooser filec = new JFileChooser();
			filec.showOpenDialog(frame);
			IFile.namefile = filec.getSelectedFile().toString();
		});
		
		istats.addActionListener(e ->{Dialogs.dialogstats();});
		
		//
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder("Пометки"));
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		frame.add(panel, bl.CENTER);
		
		//
		JTextArea tex = new JTextArea(4,0);
		tex.setLineWrap(true);
		tex.setWrapStyleWord(true);
						
		//
		JPanel panelcheckbox = new JPanel();
		panelcheckbox.setLayout(new BoxLayout(panelcheckbox, BoxLayout.Y_AXIS));
		
		JScrollPane scrollcheck = new JScrollPane(panelcheckbox);
		panel.add(scrollcheck);
		
		JPanel panelcheckboxbutt = new JPanel();
		BorderLayout blchbut = new BorderLayout();
		panelcheckboxbutt.setLayout(blchbut);
		
		JButton buttcheckadd = new JButton("Добавить");
		JTextField textcheck = new JTextField(12);
		
		JPanel buttcheckandfield = new JPanel();
		buttcheckandfield.setLayout(new BoxLayout(buttcheckandfield, BoxLayout.X_AXIS));
		
		JCheckBox maininpanelcheckbox = new JCheckBox();
		
		buttcheckandfield.add(maininpanelcheckbox);
		buttcheckandfield.add(textcheck);
		buttcheckandfield.add(buttcheckadd);
		
		//
		panelcheckboxbutt.add(buttcheckandfield, blchbut.NORTH);
		
		JSeparator sepcheck = new JSeparator();
		sepcheck.setBorder(BorderFactory.createLineBorder(Color.lightGray,2));
		sepcheck.setLayout(new BorderLayout());
		JLabel textsepcheck = new JLabel("Добавьте или удалите пометку");
		textsepcheck.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		sepcheck.add(textsepcheck, BorderLayout.CENTER);
		
		
		panelcheckboxbutt.add(sepcheck, blchbut.CENTER);
		
		//TOP
		panelcheckbox.add(panelcheckboxbutt, BoxLayout.X_AXIS);
		
		List<JPanel> panellischeck = new ArrayList<>();
		List<JCheckBox> lischeck = new ArrayList<>();
		
		buttcheckadd.addActionListener(e -> {
			JPanel panelcheckboxadd = new JPanel();
			panelcheckboxadd.setLayout(new BorderLayout());
			
			JCheckBox checkbox  = new JCheckBox();
			checkbox.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
			checkbox.setForeground(new Color(36, 61, 76));
			checkbox.setSelected(maininpanelcheckbox.isSelected());
			maininpanelcheckbox.setSelected(false);
			count2++;
			
			if(textcheck.getText().equals(""))
				checkbox.setText("NoneName " + count2);
				else checkbox.setText(textcheck.getText());
			textcheck.setText("");//clear field
			
			JButton buttcheckrem = new JButton("Удалить");
			buttcheckrem.setBorder(BorderFactory.createLineBorder(Color.lightGray));
			buttcheckrem.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
						
			panellischeck.add(panelcheckboxadd);
			lischeck.add(checkbox);
			
			panelcheckboxadd.add(checkbox, BorderLayout.CENTER);
			panelcheckboxadd.add(buttcheckrem, BorderLayout.EAST);

			panelcheckbox.add(panelcheckboxadd);
			
			textsepcheck.setText("");
			
			panelcheckbox.revalidate();
			panelcheckbox.repaint();
			
			buttcheckrem.addActionListener(ee ->{
				
				if(count2>0){
					int size = 0; //Господи, прости за быдло код
					for(int i=0; i<lischeck.size();i++)if(lischeck.get(i).getText().equals(checkbox.getText()))size=i;	
					
					panelcheckbox.remove(panellischeck.get(size));
					panellischeck.remove(size);
					lischeck.remove(size);					
					
					panelcheckbox.repaint();
					panelcheckbox.revalidate();
					
					count2--;
					if(count2==0)textsepcheck.setText("Добавьте или удалите пометку");
				}
			});
		});
		
		//
		JPanel paneladd = new JPanel();
		
		paneladd.setLayout(new BoxLayout(paneladd, BoxLayout.Y_AXIS));
		
		JScrollPane scrolladd = new JScrollPane(paneladd);
		scrolladd.setBorder(BorderFactory.createTitledBorder("Доп.Пометки"));
		
		JButton buttonadd = new JButton("Добавить");
		
		JPanel paneladdbutton = new JPanel();
		BorderLayout blpab = new BorderLayout();
		paneladdbutton.setLayout(blpab);
		
		paneladdbutton.add(buttonadd, blpab.NORTH);
		
		JSeparator sep = new JSeparator();
		sep.setBorder(BorderFactory.createLineBorder(Color.lightGray,2));
		sep.setLayout(new BorderLayout());
		JLabel septext = new JLabel("Добавьте или удалите текстовое поле");
		sep.add(septext, BorderLayout.CENTER);
		paneladdbutton.add(sep, blpab.CENTER);
			
		buttonadd.setBorder(null);
		
		
		paneladd.add(paneladdbutton, BoxLayout.X_AXIS);
		
		panel.add(scrolladd,bl.SOUTH);
		
		//BOTTOM
		List<JPanel> lispanel = new ArrayList<>();
		List<JTextField> listext = new ArrayList<>();
		
		
		buttonadd.addActionListener(e -> {
			
			JPanel paneltextfieldadd = new JPanel();
			paneltextfieldadd.setLayout(new BorderLayout());
			
			JTextField textfieldadd = new JTextField();
			textfieldadd.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
			textfieldadd.setBackground(new Color(224, 235, 239));
			textfieldadd.setForeground(new Color(36, 61, 76));
			paneltextfieldadd.add(textfieldadd, BorderLayout.CENTER);
			
			JButton buttonrem = new JButton("Удалить");
			buttonrem.setFont(new Font("Comic Sans MS", Font.BOLD, 12));
			buttonrem.setBorder(BorderFactory.createLineBorder(Color.lightGray));
			
			paneltextfieldadd.add(buttonrem, BorderLayout.EAST);
			
			textfieldadd.setText(count+". ");
			count++;
			
			listext.add(textfieldadd);
			lispanel.add(paneltextfieldadd);
			
			paneladd.add(paneltextfieldadd);
			
			septext.setText("");
			
			paneladd.revalidate();
			paneladd.repaint();
			
			buttonrem.addActionListener(ee->{
				
				if(count>1){
					int size = 0;
					for(int i=0; i<listext.size();i++)if(listext.get(i).getText().equals(textfieldadd.getText())){size=i;}
					
					paneladd.remove(lispanel.get(size));
					lispanel.remove(size);	
					listext.remove(size);
					
					paneladd.repaint();
					paneladd.revalidate();
					
					count--;
					if(count==1)septext.setText("Добавьте или удалите текстовое поле");
				}
			});
		});
		
		//
		JPanel paneldown = new JPanel();
		
		JButton button = new JButton("+");
		button.setFont(new Font("Arial",Font.BOLD,40));
		button.setBorder(null);
		
		List<String> lisfield = new ArrayList<>();
		List<String> lischecktext = new ArrayList<>();
		
		button.addActionListener(e->{
			DateFormat dateformat = new SimpleDateFormat("dd/MM/yy HH:mm");
			Date date = new Date();
			String timestring = dateformat.format(date);
			
			for(int i=0; i<lischeck.size();i++)lischecktext.add(lischeck.get(i).getText()+"~"+lischeck.get(i).isSelected());
			for(int i=0; i<lispanel.size();i++)lisfield.add(listext.get(i).getText());
			
			String[] tmp = {timestring, tex.getText().trim()};
			IFile.filewrite(tmp, lischecktext, lisfield);
			
			//Clear
			
			tex.setText(null);
			for(int i=0; i<lispanel.size();i++) paneladd.remove(lispanel.get(i));
			lispanel.clear();
			listext.clear();
			lisfield.clear();
			paneladd.repaint();
			septext.setText("Добавьте или удалите текстовое поле");
			count=1;
			
			});
		
		paneldown.setLayout(new BorderLayout());
		paneldown.setBorder(BorderFactory.createTitledBorder("Заметка"));
		
		JScrollPane scroll = new JScrollPane(tex, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		paneldown.add(scroll, bl.CENTER);
		paneldown.add(button,bl.EAST);
		
		frame.add(paneldown, bl.SOUTH);
		
		//graphics
		
		Font font = new Font("Verdana", Font.BOLD, 10);
		jmfile.setFont(font); jmtools.setFont(font);
		isave.setFont(font); iopen.setFont(font);
		istats.setFont(font);
		
		Font fontall = new Font("Comic Sans MS", Font.BOLD, 13);
		buttcheckadd.setFont(fontall);
		buttonadd.setFont(fontall);
		textsepcheck.setFont(fontall);
		septext.setFont(fontall);
		
		Color coltex = new Color(36, 61, 76);
				
		textcheck.setBackground(new Color(224, 235, 239));
		textcheck.setForeground(coltex);
		textcheck.setFont(fontall);
						
		tex.setBackground(new Color(224, 235, 239));
		tex.setForeground(coltex);
		tex.setFont(fontall);
	
	}
}