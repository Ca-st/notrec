import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.*;

public class Dialogs {
	
	static Gui gg = new Gui();
	static JDialog dialog;
	
	public static void dialogstats(){
		dialog = new JDialog(gg.frame, "Statistics");
		dialog.setSize(700, 300);		
		dialog.setVisible(true);
		BorderLayout bl = new BorderLayout();
		dialog.setLayout(bl);
		dialog.setResizable(false);
					
		if(Files.notExists(IFile.path)){
				JLabel labelwarn = new JLabel("НЕТ СТАТИСТИКИ!");
				labelwarn.setBorder(BorderFactory.createLineBorder(Color.red,4));
				labelwarn.setFont(new Font("Arial", Font.BOLD, 25));
				dialog.add(labelwarn, bl.CENTER);
				dialog.pack();
			}else{
				
				JPanel paneldown = new JPanel();
				paneldown.setLayout(new BorderLayout());
				
				IFile.readfile();
				
				String[] nametable = {"Время", "Пометки", "Доп.Пометки", "Заметка"};
				String[][] stringtable = new String[IFile.list.size()][4];
				
				//
				for(int i =0; i<IFile.list.size(); i++)
						for(int a=0; a<4;a++) stringtable[i][a]=IFile.list.get(i).split("\\|", -1)[a];
			
				JTable table = new JTable(stringtable, nametable);
				table.setEnabled(false);
				table.getTableHeader().setReorderingAllowed(false);
				
				JScrollPane scrollpaneldown = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				
				dialog.add(scrollpaneldown, BorderLayout.CENTER);
								
				dialog.add(paneldown, BorderLayout.SOUTH);
			}
			
		
		
	}
}

