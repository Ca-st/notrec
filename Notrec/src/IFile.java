import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class IFile {
	
	static String namefile = "file.txt";
	static List<String> list;
	static Path path = Paths.get(namefile);
	
	public static void filewrite(String[] tmp, List lischeck, List lisfield){
		try{	
			readfile();
			
			//Время[0]|MULTICHECK|MULTIFIELD|Заметка[1]			
			String tm = tmp[0]+lischeck.toString().replace("[", "|").replace("]", "")
						+lisfield.toString().replace("[", "|").replace("]", "|")
						+ tmp[1].toString();
			
			list.add(tm);
			Files.write(path, list);
			
		}catch(Exception ex){ex.printStackTrace();}
	}
	
	public static void readfile(){
		try{
			if(Files.notExists(path))Files.createFile(path);			
			list = Files.readAllLines(path, StandardCharsets.UTF_8);
		}catch(Exception ex){ex.printStackTrace();}
	}
	
	
	static String nameconfig = "config.txt";
	
	public static void configfile(String con){
		
		try{
			Path path = Paths.get(nameconfig);
			if(Files.notExists(path)) Files.createFile(path);
			
			List<String> list = Files.readAllLines(Paths.get(nameconfig), StandardCharsets.UTF_8);
			
			//Файл БД[0]|
			String configstring = ""; 
			list.add(configstring);
			Files.write(Paths.get(nameconfig), list);
			
		}catch(Exception ex){ex.printStackTrace();}
		}
}
